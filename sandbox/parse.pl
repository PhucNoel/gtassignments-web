#!/usr/bin/perl
use strict;
use warnings;

use Getopt::Std;
use WWW::Mechanize;
use HTTP::Cookies;
use LWP::Debug qw(+);
use HTML::SimpleLinkExtor;
use Data::Dumper;
use HTML::TableExtract;
sub usage(;$)
{
    my( $msg ) = @_;
    print "$msg\n" if( $msg );
    print "Usage:\n";
    print " $0 -u username -p password\n";
    exit 1;
}

our( $opt_u, $opt_p );
die ( "Please enter correct login information" )unless( getopts( 'u:p:' ) );

my $username = $opt_u;
my $password = $opt_p;

my $outfile = "out.html";

my $url = "https://t-square.gatech.edu/portal";
my $mech = WWW::Mechanize->new();
$mech->cookie_jar(HTTP::Cookies->new());
$mech->get($url);

$mech->follow_link(text => "Login", n => 1);   
$mech->submit_form(
    form_id=> 'fm1',
    fields => { username    => $username,
                password    => $password
              },
    button => 'submit',
);
$mech->follow_link(text => "CS-2200-A,GR SUM13", n => 1); 
$mech->follow_link(text => "Assignments", n => 1);
my $output_page = $mech->content();
my $extor = HTML::SimpleLinkExtor->new();
$extor->parse( $output_page );
my @frame_links = $extor->iframe;
$mech->follow_link(url => @frame_links);
$output_page = $mech->content();

open(OUTFILE, ">$outfile");
print OUTFILE "$output_page";
close(OUTFILE);
