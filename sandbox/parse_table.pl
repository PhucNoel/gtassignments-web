#!/usr/bin/perl
use strict;
use warnings;

use HTML::TableExtract;

# Exactract table from html file
my $te = new HTML::TableExtract( attribs => { border => 0, cellspacing => 0} );
$te->parse_file("out.html");
my ($table) = $te->tables;
my @rows = $table->rows;

foreach my $row (@rows[1 .. $#rows]) {
    # trim leading/trailing whitespace from base fields
    s/^\s+//, s/\s+$// for @$row;
    # print join(',', @$row), "\n";
    print $row->[3];
}



