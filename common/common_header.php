<?php

global $webroot;
global $pk_entity;

$pk_entity = -1;
$webroot   = substr(__FILE__, 0, strpos(__FILE__, '/common/includes/common_header.php' ) );

require_once( $webroot . '/common/functions/ui_lib.php' );

if( !( defined( 'ALLOW_INSECURE' ) && constant( 'ALLOW_INSECURE' ) ) )
{
    check_for_ssl();
}

require_once( $webroot . '/common/functions/authenticate.php'   );
require_once( $webroot . '/common/functions/includes.php'       );
require_once( $webroot . '/common/functions/util.php'           );
require_once( $webroot . '/common/functions/validate_lib.php'   );

if( $pk_entity < 0 )
{
    if( !( defined( 'NO_AUTHENTICATION_REQUIRED' ) && constant( 'NO_AUTHENTICATION_REQUIRED' ) ) )
    {
        if( array_key_exists( 'HTTP_HOST', $_SERVER ) )
        {
            header( "Location: ". NAV_LOGIN . '?' . urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) );
        }
else
        {
            header( "Location: ". NAV_LOGIN );
        }

        echo( 'false:Not logged in.' );
        exit;
    }
}

function insert_header()
{
    global $webroot, $pk_entity, $entity_name, $entity;
    require_once( $webroot . '/common/includes/header.php' );
}

function insert_footer()
{
    global $webroot, $pk_entity, $entity_name, $entity;
    require_once( $webroot . '/common/includes/footer.php' );
}

?>

