<?php

//require_once( 'config.php' );

// Replace all "__VARIABLES__" with $variables in a string (see example below).
// Used in conjuction with translate because you can't put PHP variable names inside translate.
//
// Example:
//
// $orderid = 123;
// $pk_project = 45;
// $project_name = "Sign Refresh in Office Demo";
//
// $string = "Your order #__ORDERID__ has bee updated for project # __PK_PROJECT__ (__PROJECT_NAME__)."
// $string = replace_string_with_variable( $string, get_defined_vars() );
//
//
// $local_variables is an array of variables that will do the replacing.
// Typically use PHP's get_defined_vars() to pass for $local_variables.
function replace_string_with_variable( $string, $local_variables )
{
    preg_match_all( "/__([A-Z_]+)__/", $string, $matches );

    for( $i = 0; $i < count( $matches[0] ); $i++ )
    {
        $variable = strtolower( $matches[1][$i] );
        $string = str_replace( $matches[0][$i], $local_variables[ $variable ], $string );
    }

    return $string;
}

/**
 * Gets data from a specified URL
 * @param  String $url The URL that needs to be cURL'd
 * @return Mixed       The data
 */
function curl_url( $url )
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

/**
 * Consolidated checks for uploading files.
 *
 * @param  PHPVAR       $attachment            $_FILES variable
 * @param  String       $user_file_key         The key passed into the $_FILES variable (the name of the form element which uploads files)
 * @param  String       $destination_directory The destination directory to upload the file to
 * @param  Int          $file_size_check       File size to check in MB
 * @param  String/Array $allowed_extensions    Takes in a string of predefined allowed extensions, or an array of allowed extensions
 * @return String/Null                         Returns a string if there was an error, null on success.
 */
function upload_single_file_error_checks(
                                            $attachment,
                                            $user_file_key,
                                            $destination_directory,
                                            $file_size_check,
                                            $allowed_extensions = null
                                        )
{
    $mb_to_bytes = 1048576;

    if( !is_null( $allowed_extensions ) and is_string( $allowed_extensions ) )
    {
        $allowed_extensions_strings = explode( ',', $allowed_extensions );
        $allowed_extensions         = array();

        if( ( $key = array_search( 'images', $allowed_extensions_strings ) ) !== false )
        {
            $allowed_extensions = array_merge( $allowed_extensions, array( 'jpg', 'jpeg', 'gif', 'png', 'bmp', 'tif', 'tiff' ) );
            unset( $allowed_extensions_strings[ $key ] );
        }

        if( ( $key = array_search( 'documents', $allowed_extensions_strings ) ) !== false )
        {
            $allowed_extensions = array_merge( $allowed_extensions, array( 'doc', 'docx', 'xls', 'xlsx', 'csv', 'pdf' ) );
            unset( $allowed_extensions_strings[ $key ] );
        }

        foreach( $allowed_extensions_strings as $string )
        {
            array_push( $allowed_extensions, $string );
        }
    }

    if( !array_key_exists( $user_file_key, $_FILES ) or empty( $_FILES[ $user_file_key ] ) )
    {
        return 'No file was attached';
    }

    $attachment = $_FILES[ $user_file_key ];

    if( $attachment['error'] !== UPLOAD_ERR_OK )
    {
        /*
        ERROR CODES FROM http://php.net/manual/en/features.file-upload.errors.php
        */
        $error_message = "Failed to upload file. Reason: ";

        switch( $attachment[ 'error' ] )
        {
            case UPLOAD_ERR_INI_SIZE:
                $error_message .= "The upload file exceeds the server's allowed filesize.";
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $error_message .= "The uploaded file exceeds the max file size specified in the HTML form.";
                break;
            case UPLOAD_ERR_PARTIAL:
                $error_message .= "The uploaded file was partially uploaded.";
                break;
            case UPLOAD_ERR_NO_FILE:
                $error_message .= "No file was selected.";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $error_message .= "Missing temporary folder. Please contact support about this error.";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $error_message .= "Failed to write file to disk. Please contact support about this error.";
                break;
            case UPLOAD_ERR_EXTENSION:
                $error_message .= "Extension stopped the file upload. Please contact support about this error.";
                break;
        }

        return $error_message;
    }

    if( $attachment['size'] <= 0 )
    {
        return 'Invalid file was attached';
    }

    if( $attachment['size'] > $file_size_check * $mb_to_bytes )
    {
        return "File cannot be greater than $file_size_check MB";
    }

    if( !is_uploaded_file( $attachment[ 'tmp_name' ] ) )
    {
        return 'Failed security check. Not an uploaded file.';
    }

    /*
    if( !pass_mime_type_checks( $attachment[ 'name' ] ) )
    {
        $ext = pathinfo( $attachment['name'], PATHINFO_EXTENSION );
        return "Failed to pass MIME type check. Extension '$ext' and MIME type '{$attachment['type']}' do not match.";
    }
    */

    if( !is_dir( $destination_directory ) )
    {
        if( !@mkdir( $destination_directory, 0770, true ) )
        {
            if( !is_dir( $destination_directory ) )
            {
                return 'Failed to create the destination directory';
            }
        }
    }

    $pathinfo = pathinfo( $attachment['name'] );

    if( !( isset( $pathinfo['extension'] ) && array_key_exists( 'extension', $pathinfo ) ) )
    {
        return "File does not have an extension.";
    }

    if( is_array( $allowed_extensions ) )
    {
        $temp_array = array();

        foreach( $allowed_extensions as $extension )
        {
            array_push( $temp_array, strtolower( $extension ) );
        }

        $allowed_extensions = $temp_array;
        $file_extension     = strtolower( $pathinfo[ 'extension' ] );

        if( strlen( $file_extension ) > 5 ) // malicious code in extensions
        {
            return "Extension too long.";
        }

        if( !in_array( $file_extension, $allowed_extensions ) )
        {
            return "Extension not allowed. Types of files allowed: " . implode( ', ', $allowed_extensions ) . '.';
        }
    }

    return null;
}

/**
 * Checks to see if the MIME type of the file matches the extension.
 * This is to prevent faking of extensions. We must maintain the $mime_type_array
 * association ourselves.
 *
 * @param  String $file_path Path to the file on disk.
 * @return Bool              Return true if the MIME type matches the extension, false if it doesn't.
 *                           Example: If "test.pdf" has a MIME type of 'application/pdf', this function will return true.
 */
function pass_mime_type_checks( $file_name_or_path )
{
    $extension = pathinfo( $file_name_or_path, PATHINFO_EXTENSION );
    $finfo     = finfo_open( FILEINFO_MIME_TYPE );

    $mime_type_array = array(
        'pdf'  => 'application/pdf',
        'xls'  => 'application/vnd.ms-excel',
        'doc'  => 'application/msword',
        'ppt'  => 'application/vnd.ms-powerpoint',
        'xlsx' => 'application/zip',
        'docx' => 'application/zip',
        'pptx' => 'application/zip',
        'txt'  => 'text/plain',
        'csv'  => 'text/csv',
        'png'  => 'image/png',
        'bmp'  => 'image/bmp',
        'gif'  => 'image/gif',
        'jpe'  => 'image/jpeg',
        'jpg'  => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'pic'  => 'image/pict',
        'pict' => 'image/pict',
        'ps'   => 'application/postscript',
        'rtf'  => 'text/rtf',
        'svg'  => 'image/svg+xml',
        'tif'  => 'image/tiff',
        'tiff' => 'image/tiff',
        'zip'  => 'application/zip'
    );

    if( !$finfo )
    {
        return "Opening fileinfo database failed";
    }

    $mime_type = finfo_file( $finfo, $file_path );
    finfo_close( $file_info );

    if( $mime_type_array[$extension] !== $mime_type )
    {
        return false;
    }

    return true;
}

/**
 * Cleans a PDF by fixing xref table errors that are output by Ghostscript
 *
 * @param  String $input_file_path  Input file path
 * @param  String $output_file_path Output file path
 * @return String/Null              On success, return null. On failure, return a string with the error message.
 */
function clean_pdf( $input_file_path, $output_file_path, $check_extension = true )
{
    if( !file_exists( $input_file_path ) )
    {
        return "File does not exist";
    }

    if( $input_file_path === $output_file_path )
    {
        return "Input and Output file names cannot be the same";
    }

    if( $check_extension and ( pathinfo( $input_file_path, PATHINFO_EXTENSION ) != 'pdf' ) )
    {
        return "Input file $input_file_path did not have a .pdf extension";
    }

    $finfo = finfo_open( FILEINFO_MIME_TYPE );

    if( finfo_file( $finfo, $input_file_path ) != 'application/pdf' )
    {
        finfo_close( $finfo );
        return "File $input_file_path did not have a MIME type of application/pdf";
    }

    $command = "pdftk $input_file_path output $output_file_path";

    $errors = execute_system_command_return_errors( $command );

    if( !file_exists( $output_file_path ) )
    {
        finfo_close( $finfo );
        return "Failed to create output file $output_file_path, file does not exist";
    }

    $mime = finfo_file( $finfo, $output_file_path );

    if( $mime != 'application/pdf' )
    {
        finfo_close( $finfo );
        return "Output file $output_file_path did not have a MIME type of application/pdf";
    }

    finfo_close( $finfo );
    return null;
}

/**
 * Merges two or more PDFs together
 *
 * @param  Array       $arguments Array of file paths. First element in the array is the path to the output file. N-th arguments are the paths to files that need to be merged.
 * @return String/Null            On success, return null. On failure, return a string with the error message.
 */
function merge_pdfs( $arguments )
{
    if( !is_array( $arguments ) )
    {
        return "Arguments were not in array format : debug_dump -- " . debug_dump( $arguments );
    }

    if( count( $arguments ) < 3 )
    {
        return "Failed to merge PDFs, invalid number of arguments (array of 3 arguments required).";
        debug( "Failed to merge PDFs, invalid number of arguments (array of 3 arguments required)." );
    }

    $finfo    = finfo_open( FILEINFO_MIME_TYPE, '/usr/share/misc/magic.mgc' );
    $out_file = $arguments[0];

    if( pathinfo( $out_file, PATHINFO_EXTENSION ) != 'pdf' )
    {
        finfo_close( $finfo );
        return "Output file $out_file did not have a .pdf extension";
        debug( "Output file $out_file did not have a .pdf extension" );
    }

    //$command = "gs -q -sPAPERSIZE=letter -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$out_file ";
    $command = "pdftk ";

    for( $i = 1; $i < count( $arguments ); $i++ )
    {
        if( !file_exists( $arguments[$i] ) )
        {
            $arg_str = implode( ' ', $arguments );
            error_log( "merge_pdfs(): file $arguments[$i] is missing, args were {$arguments}" );
            return "File {$arguments[$i]} does not exist.";
        }

        if( pathinfo( $arguments[$i], PATHINFO_EXTENSION ) != 'pdf' )
        {
            finfo_close( $finfo );
            return "File {$arguments[$i]} did not have a .pdf extension";
        }

        $file_mime = finfo_file( $finfo, $arguments[$i] );

        if( $file_mime === false )
        {
            finfo_close( $finfo );
            return "Failed to open file {$arguments[$i]}.";
        }

        if( $file_mime != 'application/pdf' )
        {
            finfo_close( $finfo );
            $archive_file = '/tmp/' . uniqid( 'merge_pdf_archive_', true ) . '.pdf';
            copy( $arguments[$i], $archive_file );
            return "File {$arguments[$i]} did not have a MIME type of application/pdf (had type $file_mime), archived in $archive_file";
        }

        $command .= $arguments[$i] . ' ';
    }

    $command .= " cat output $out_file";
    $retval = execute_system_command_return_errors( $command );

    if( !file_exists( $out_file ) )
    {
        finfo_close( $finfo );
        return "Failed to create output file $out_file, file does not exist.";
    }

    $mime = finfo_file( $finfo, $out_file );

    if( $mime != 'application/pdf' )
    {
        finfo_close( $finfo );
        return "Output file $out_file did not have a MIME type of application/pdf";
    }

    // Clean up extra PDFs after merge if they're in the tmp directory.
    // We shift off the first arg because it is the output file, and should not be deleted.
    array_shift( $arguments );
    foreach( $arguments as $pdf_to_cleanup )
    {
        if( file_exists( $pdf_to_cleanup ) and preg_match( '/^\/tmp/', $pdf_to_cleanup ) and !preg_match( '/^\/tmp\/data/', $pdf_to_cleanup ) )
        {
            @unlink( $pdf_to_cleanup );
        }
    }

    finfo_close( $finfo );
    return null;
}

function add_to_map( &$map, $hash, $key )
{
    $map[$key] = null;

    if( array_key_exists( $key, $hash ) )
    {
        $map[$key] = $hash[$key];
        return true;
    }
    return false;
}

function add_to_map_if_set( &$map, $hash, $key )
{
    if( array_key_exists( $key, $hash ) )
    {
        if( ( $hash[ $key ] != null ) and ( $hash[ $key ] != '' ) )
        {
            $map[ $key ] = $hash[ $key ];
            return true;
        }
    }

    return false;
}

function add_string_to_map_if_set( &$map, $hash, $key, $max_length )
{
    if( array_key_exists( $key, $hash ) )
    {
        if( is_null( $hash[ $key ] ) )
        {
            return "Invalid $key provided";
        }
        else if( strlen( $hash[ $key ] ) > $max_length )
        {
            return "$key must be less than $max_length characters";
        }
        else
        {
            $map[ $key ] = $hash[ $key ];
            return true;
        }
    }

    return false;
}

function add_integer_to_map_if_set( &$map, $hash, $key, $zero_allowed )
{
    if( array_key_exists( $key, $hash ) )
    {
        $value = $hash[ $key ];

        if( is_null( $value ) or !is_numeric( $value ) )
        {
            return "Invalid $key provided";
        }
        else
        {
            if( $value == 0 and !$zero_allowed )
            {
                return "Invalid $key provided";
            }

            $map[ $key ] = $value;
            return true;
        }
    }

    return false;
}

function add_positive_integer_to_map_if_set( &$map, $hash, $key, $zero_allowed = false )
{
    if( array_key_exists( $key, $hash ) )
    {
        $value = $hash[ $key ];

        if( is_null( $value ) or !is_numeric( $value ) )
        {
            return "Invalid $key provided";
        }
        else
        {
            if( ( $value == 0 and !$zero_allowed ) or $value < 0 )
            {
                return "Invalid $key provided";
            }

            $map[ $key ] = $value;
            return true;
        }
    }
}

function add_boolean_to_map_if_set( &$map, $hash, $key )
{
    if( array_key_exists( $key , $hash ) )
    {
        if(
            is_null( $hash[ $key ] ) or
            !(
                $hash[ $key ] === 'true'  or
                $hash[ $key ] === true    or
                $hash[ $key ] === 'false' or
                $hash[ $key ] === false   or
                $hash[ $key ] === 'f'     or
                $hash[ $key ] === 't'
            )
          )
        {
            return "Invalid $key provided";
        }

        $map[ $key ] = $hash[ $key ];
        return true;
    }

    return false;
}

function add_date_to_map_if_set( &$map, $hash, $key, $format = '/\d{4}\-\d{1,2}\-\d{1,2}/' )
{
    if( array_key_exists( $key , $hash ) )
    {
        if(
                !empty( $hash[ $key ] ) and
              ( preg_match( $format, $hash[ $key ] ) )
          )
        {
            $map[ $key ] = $hash[ $key ];
            return true;
        }
    }

    return false;
}

function add_integer_array_to_map_if_set( &$map, $hash, $key )
{
    if( array_key_exists( $key, $hash ) )
    {
        $value_array = null;

        if( is_array( $hash[ $key ] ) )
        {
            $value_array = array();
            foreach( $hash[ $key ] as $instance )
            {
                if( is_numeric( $instance ) and $instance > 0 )
                {
                    array_push( $value_array, $instance );
                }
            }
        }
        else if( is_numeric( $hash[ $key ] ) and $hash[ $key ] > 0 )
        {
            $value_array = array( $hash[ $key ] );
        }

        $map[ $key ] = $value_array;
        return true;
    }

    return false;
}

function get_tail_of_status_file( $status_file )
{
    $content = '';

    if( !file_exists( $status_file ) )
    {
        $content = "<b>Status file was not found, <br/>will check again in 2 seconds.</b>";
    }
    elseif( filesize( $status_file ) == 0 )
    {
        $content = "Status file is <b>empty</b>, waiting for script to start.";
    }
    else
    {
        $lines = file( $status_file );
        $start = max( 0, count( $lines ) - 8 );

        for( $i = $start; ($i < count($lines)) ; $i++ )
        {
            $content .= $lines[$i] . "<br/>";
        }

    }

    return $content;
}

function execute_system_command_return_errors()
{
    $args            = func_get_args();
    $arg_count       = func_num_args();
    $escaped_command = '';
    $error_messages  = array();

    if( $arg_count == 1 )
    {
        $escaped_command = escapeshellcmd( $args[0] );
    }
    else
    {
        foreach( $args as $arg )
        {
            $escaped_command .= escapeshellarg( $arg ) . ' ';
        }
    }

    $redirected_command = "$escaped_command 2>&1 >/dev/null";
    $exception          = '';

    try
    {
        $script_output = array();
        $script_retval = null;
        exec( $redirected_command, $script_output, $script_retval );
    }
    catch( Exception $e )
    {
        error_log( 'Exception ' . $e->getMessage() . " while executing $escaped_command" );
        $exception = ' (Exception: ' . $e->getMessage() . ')';
        return array( false, array( $exception ) );
    }

    if( $script_retval == 0 )
    {
        return array( true, $script_output );
    }

    return array( false, $script_output );
}

function execute_system_command()
{
    $args           = implode( ' ', func_get_args() );
    $out_array      = execute_system_command_return_errors( $args );
    $result         = $out_array[0];
    $error_messages = $out_array[1];

    foreach( $error_messages as $error_message )
    {
        error_log( $error_message );
    }

    return $result;
}

function debug( $msg )
{
    if ( is_production() or is_staging() )
    {
        // Do not log on production or staging
        return;
    }
    error_log( $msg );
}

function debug_dump( $structure )
{
    ob_start();
    var_dump($structure);
    debug('The structure is: ' . ob_get_contents());
    ob_end_clean();
}

function debug_dump_query( $query, $params )
{
    $debug = $query . ' (';

    foreach( $params as $param => $value )
    {
        $debug .= "$param = $value, ";
    }

    $debug = trim( $debug, ', ' );

    debug( $debug . ')' );
}

function debug_dump_json( $value, $prett_print )
{
    $options = 0;

    if( $prett_print )
    {
        $options = JSON_PRETTY_PRINT;
    }

    debug( json_encode( $value, $options ) );
}

function generate_password()
{
    $length        = 8;
    $password      = "";
    $random_number = 0;

    for( $i = 0; $i < $length; $i++ )
    {
        $random_number = get_random_number();

        while( is_invalid_character( $random_number ) )
        {
            $random_number = get_random_number();
        }

        $password .= chr( $random_number );
    }

    return $password;
}

function get_random_number()
{
    return ( intval( rand( 0, 1000 ), 10 ) % 94 ) + 33;
}

function is_invalid_character( $num )
{
    $rejected_characters = array( 73, 105, 76, 108, 49, 79, 111, 48 ); // Don't use I, i, l, L, 1, O, o, 0

    if( in_array( $num, $rejected_characters ) )
    {
        return true;
    }

    if( ( $num >= 33 ) && ( $num <= 47 ) )
    {
        return true;
    }

    if( ( $num >= 58 ) && ( $num <= 64 ) )
    {
        return true;
    }

    if( ( $num >= 91 ) && ( $num <= 96 ) )
    {
        return true;
    }

    if( ( $num >= 123 ) && ( $num <= 126 ) )
    {
        return true;
    }

    return false;
}

function get_numeric_keypress_handler( $allow_negative, $allow_fractional )
{
    if( $allow_fractional == 'f' )
    {
        if( $allow_negative == "t" )
        {
            $quantity_key_handler = "numeric_integer_only(event);";
        }
        else
        {
            $quantity_key_handler = "numeric_positive_integer_only(event);";
        }
    }
    else
    {
        if( $allow_negative == "t" )
        {
            $quantity_key_handler = "numeric_real_hundredths_only(event,this);";
        }
        else
        {
            $quantity_key_handler = "numeric_positive_real_hundredths_only(event,this);";
        }
    }
    return $quantity_key_handler;
}

/**
 * Convert the difference between two dates to an interval
 * @param  {Integer} diff Timestamp between two points in time, for example, now() and "5/14/2013"
 * @return {Array}        Array with hours, minutes, and seconds
 */
function convert_time_diff_to_interval( $diff )
{
    $hours   = floor($diff / 3600);
    $time    = $diff - $hours * 3600;
    $minutes = floor($time / 60);
    $seconds = $time - $minutes * 60;

    $interval = array( 'hours' => $hours, 'minutes' => $minutes, 'seconds' => $seconds );

    return $interval;
}

?>
