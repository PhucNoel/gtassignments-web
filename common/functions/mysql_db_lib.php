<?php
require_once 'constants.php';
global $con;
$con = mysql_connect( DB_HOSTNAME, DB_USERNAME, DB_PASSWORD );
mysql_select_db( DATABASE, $con );

function verify_connection()
{
    global $con;

    if( !is_resource( $con ) )
    {
        error_log( 'Database $con is not valid!' );
        return false;
    }

    return true;
}

function get_last_db_error()
{
    global $con;

    if( !verify_connection() )
    {
        return '$con is not valid!';
    }

    $retval = mysql_error( $con );

    return $retval;
}

function prepare_param_query( &$query, &$params )
{
    $param_index = 1;
    $inserted_params = array();

    foreach( $params as $key => $param )
    {
        $split_query = explode( '?' . $key . '?', $query );
        $num = count( $split_query );

        if( $num === 1 )
        {
            continue;
        }

        $param_str = '';

        if( is_array( $param ) )
        {
            foreach( $param as $value )
            {
                if( $value === true )
                {
                    $value = 't';
                }
                elseif( $value === false )
                {
                    $value = 'f';
                }

                $param_str .= '$' . $param_index . ',';
                array_push( $inserted_params, $value );
                $param_index++;
            }

            # Strip off final comma
            $param_str = substr( $param_str, 0, -1 );
        }
        else
        {
            if( $param === true )
            {
                $param = 't';
            }
            elseif( $param === false )
            {
                $param = 'f';
            }

            $param_str = '$' . $param_index;
            array_push( $inserted_params, $param );
            $param_index++;
        }

        $query = $split_query[0];

        for( $i = 1; $i < $num; $i++ )
        {
            $query .= $param_str . $split_query[$i];
        }
    }

    $param_index--;

    $params = $inserted_params;

    # Find any unsubstituted parameters in query, use last $param_index for each
    # which will reference the null at the end of the params

    $query = preg_replace_callback( '/\?[0-9a-zA-Z_]+\?/',
        function( $matches ) use ( &$params, &$param_index )
        {
            array_push( $params, null );
            $param_index++;
            return '$' . $param_index;
        }, $query );

    debug( $query );
    
    return null;
}

function my_pg_query_params( $con, $query, $params )
{
    global $last_database_result;

    $time_begin     = microtime( true );
    $retry_counter  = 3;
    $try_count      = 0;
    $query_complete = false;

    while( $retry_counter > 0 )
    {
        $try_count++;
        
        if( $try_count > 1 )
        {
            sleep( rand( 0, 3 ) );
        }
        
        debug( $query );

        if( pg_send_query_params( $con, $query, $params ) === false )
        {
            error_log_backtrace( $query . ': ' . get_last_db_error(), 2 );
            return false;   
        }

        $result = pg_get_result( $con );
        $last_database_result = $result;
        
        if( pg_result_error( $result ) )
        {
            if( pg_result_error_field( $result, PGSQL_DIAG_SQLSTATE ) == '40P01' ) // Deadlock
            {
                $retry_counter--;
            }
            else
            {
                error_log_backtrace( $query . ': ' . get_last_db_error(), 2 );
                return false;
            }
        }
        else
        {
            $retry_counter  = 0;
            $query_complete = true;
        }
    }
  
    return $result;
}


function param_query_blind( $query, $params = array() )
{
    global $con;

    if( !verify_connection() )
    {
        return false;
    }

    prepare_param_query( $query, $params );

    return my_pg_query_params( $con, $query, $params );
}

function param_query_blind_return_null_or_error( $query, $params = array(), $itemtext = '' )
{
    $result = param_query_blind( $query, $params );

    if( $result === false )
    {
        if( $itemtext )
        {
            return "failed to $itemtext: $result : " . get_last_db_error();
        }
        else
        {
            return "$result : " . get_last_db_error();
        }
    }

    return null;
}

//function debug( $msg )
//{
//    error_log( $msg );
//}
//
//function debug_dump( $structure )
//{
//    ob_start();
//    var_dump($structure);
//    debug('The structure is: ' . ob_get_contents());
//    ob_end_clean();
//}
//
//function debug_dump_query( $query, $params )
//{
//    $debug = $query . ' (';
//
//    foreach( $params as $param => $value )
//    {
//        $debug .= "$param = $value, ";
//    }
//
//    $debug = trim( $debug, ', ' );
//
//    debug( $debug . ')' );
//}

function my_mysql_query( $query )
{

    $result = mysql_query( $query );

    if( !$result )
    {
        return null;
    }
    
    return $result;
}

function query_associative_all( $query, &$row_count = -1 )
{

    global $con;
    if( !verify_connection() )
    {
        $row_count = -1;
        debug( 'Connection Error' );
        return null;
    }

    if( !($result = mysql_query( $query ) ) )
    {
        debug( "Query: $query " . get_last_db_error() );
        return null;
    }

    $row_count = mysql_num_rows( $result );
    $retval = mysql_fetch_all_associative( $result ); 
    return $retval; 
}

function query_blind( $query )
{
    global $con;

    if( !verify_connection() )
    {
        return false;
    }

    return mysql_query( $query );
}

function query_blind_return_null_or_error( $query, $itemtext = '' )
{
    $result = query_blind( $query );

    if( $result === false )
    {
        if( $itemtext )
        {
            return "failed to $itemtext: $result : " . get_last_db_error();
        }
        else
        {
            return "$result : " . get_last_db_error();
        }
    }

    return null;

}


function mysql_fetch_all_associative( $result )
{
    return mysql_fetch_all( $result, MYSQL_ASSOC );
}

/**
 * Fetches all rows from a MySQL result set as an array of arrays
 *
 * Requires PHP >= 4.3.0
 *
 * @param   $result       MySQL result resource
 * @param   $result_type  Type of array to be fetched
 *                        { MYSQL_NUM | MYSQL_ASSOC | MYSQL_BOTH }
 * @return  mixed
 */
function mysql_fetch_all ($result, $result_type = MYSQL_BOTH)
{
    if (!is_resource($result) || get_resource_type($result) != 'mysql result')
    {
        trigger_error(__FUNCTION__ . '(): supplied argument is not a valid MySQL result resource', E_USER_WARNING);
        return false;
    }

    if (!in_array($result_type, array(MYSQL_ASSOC, MYSQL_BOTH, MYSQL_NUM), true))
    {
        trigger_error(__FUNCTION__ . '(): result type should be MYSQL_NUM, MYSQL_ASSOC, or MYSQL_BOTH', E_USER_WARNING);
        return false;
    }
    $rows = array();

    while ($row = mysql_fetch_array($result, $result_type))
    {
        $rows[] = $row;
    }
    return $rows;
}
?> 
