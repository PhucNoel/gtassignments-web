<?php

function db_lib()
{
    $params = func_get_args();
    $webroot = substr(__FILE__, 0, strpos(__FILE__, '/common/functions/db_lib.php' ) );
    foreach( $params as $function )
    {
        require_once( "$webroot/common/db_lib/$function.php" );
    }
}

function debug( $msg ) 
{ 
    error_log( $msg ); 
}

function get_post( $var ) 
{ 
    return mysql_real_escape_string( $_POST[$var] ); 
}

function debug_dump( $structure )
{
    ob_start();
    var_dump($structure);
    debug('The structure is: ' . ob_get_contents());
    ob_end_clean();
}


 
?>
