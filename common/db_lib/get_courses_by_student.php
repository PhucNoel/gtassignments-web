<?php

function get_courses_by_student( $pk_student )
{
    $query  = 'SELECT DISTINCT( c.name ) as course_name, '; 
    $query .= '       c.course as pk_course ';
    $query .= '  FROM tb_course c ';
    $query .= '  JOIN tb_assignment a ';
    $query .= '    ON c.course = a.course ';
    $query .= " WHERE a.student = $pk_student ";

    return query_associative_all( $query );    
}
?>
