<?php
require_once 'get_courses_by_student.php';
require_once 'get_assignments_by_student_and_course.php';

function get_assignments( $pk_student )
{
    
    $courses = get_courses_by_student( $pk_student ); 
    
    $json_array = array();

    foreach( $courses as $course )
    {
        $pk_course = $course['pk_course'];
        $course_name = $course['course_name'];
        $assignment = get_assignments_by_student_and_course( $pk_student, $pk_course );
        $json_array[$course_name] = $assignment;
    }

    return $json_array;    

} 
?>
