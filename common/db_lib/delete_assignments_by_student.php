<?php
function delete_assignments_by_student( $pk_student )
{
    $query = <<< __EOF__
    DELETE FROM tb_assignment
     WHERE student = $pk_student
__EOF__;

    $result = query_blind_return_null_or_error( $query, 'delete assignments' );

    if( !$result )
    {
        die( 'Invalid query: ' . mysql_error() );
    }

    return $result;

}
?>
