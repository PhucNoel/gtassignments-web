<?php
function get_assignments_by_student_and_course( $pk_student, $pk_course )
{
    $query  = ' select a.title as Title, ';
    $query .= '        ast.label as Status, ';
    $query .= '        a.status_modified as Updated, ';
    $query .= '        a.open_date as Open, ';
    $query .= '        a.close_date as Close ';
    $query .= '   from tb_assignment a ';
    $query .= '   join tb_assignment_status ast ';
    $query .= '     on a.assignment_status = ast.assignment_status ';
    $query .= "  where student  = $pk_student ";
    $query .= "    and course = $pk_course ";
    $query .= '  order by Open desc ';

    $result = query_associative_all( $query );
 
    if( !$result )
    {
        die( 'Invalid query: ' . mysql_error() );
    }

    return $result;
}
?>
