<?php

require_once 'constants.php';
require_once '../functions/db_lib.php';
require_once 'get_assignments.php';
require_once '../functions/mysql_db_lib.php';

if (isset($_POST['username']) &&
    isset($_POST['password']) 
   )
{
    $username   = get_post('username');
    $password   = get_post('password');
}    

// Execute perl script to login to t-square and store information
$command = 'perl ' . constant('WEBROOT_DIRECTORY') . "perl/parse.pl -u $username -p $password";
$output = shell_exec( "$command" );
debug( $command );
$json_array = array();

if( $output == '-1' ) //== 'Login error' )
{

    $json_array['authentication_status'] = 'Login error';
}
else
{
    $pk_entity = $output;
    $json_array = get_assignments( $pk_entity );
    $json_array['authentication_status'] = 'Successfully';
}

debug( $username );
debug( $password );

// Error handler and send data to android. 
echo json_encode($json_array);
delete_assignments_by_student( $pk_entity );
?>
