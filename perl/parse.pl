#!/usr/bin/perl
use strict;
use warnings;

$| = 1;
use Data::Dumper;
use Getopt::Std;
use WWW::Mechanize;
use HTTP::Cookies;
use LWP::Debug qw(+);
use HTML::SimpleLinkExtor;
use Data::Dumper;
use HTML::TableExtract;
use HTML::TreeBuilder::XPath;
use Db_lib;
use Clone qw(clone);
use DBI;
use FindBin;
use lib "$FindBin::Bin";
push @Foo::ISA, 'Clone';

sub usage(;$)
{
    my( $msg ) = @_;
    print "$msg\n" if( $msg );
    print "Usage:\n";
    print " $0 -u username -p password\n";
    exit 1;
}

our( $opt_u, $opt_p, $opt_e );
die ( "Please enter correct login information" )unless( getopts( 'u:p:' ) );

my $username = $opt_u;
my $password = $opt_p;

my $url = "https://t-square.gatech.edu/portal";
my $mech = WWW::Mechanize->new( autocheck => 0 );

$mech->cookie_jar(HTTP::Cookies->new());
$mech->get($url);

$mech->follow_link(text => "Login", n => 1);   
$mech->submit_form(
    form_id=> 'fm1',
    fields => { username    => $username,
                password    => $password
              },
    button => 'submit',
);

my $output_page = $mech->content();
my $html = $output_page;
my $tree = HTML::TreeBuilder::XPath->new_from_content($html);

if ($tree->findvalues('//div[@id="login_error"]') )
{
    print -1;
    die();
}

my $dbh = DBI->connect( 'dbi:mysql:prj092713', 'noel', '1234' )
          or die "Connection Error: $DBI::errstr\n";

# Get or create new entity
my $query = 'SELECT fn_get_or_create_entity '
          . '( ?, ?, ? ) ';
          
my $sth = $dbh->prepare( $query );
$sth->execute( $username, 
               $password,
               'now()'); 

my $query_result = $sth->fetchrow_arrayref;
my $entity = $query_result->[0];

my @classes = $tree->findvalues('//div[@id="siteNav"]//a/@title');
# There is an extra classes due to HTML parser. Remove it here.
pop @classes;
my @links = clone(@classes);

my $itr = 0;

foreach my $my_link ( @classes)
{
    $my_link = $mech->find_link( text => $classes[$itr] );
    my $url = $my_link->url;
    $itr = $itr + 1;
}

# Get all links to each class home page for future use.

# Iterating through all of classes and class the assignments.
foreach my $class ( @classes ) 
{
    my $iframe_data = undef;
    my $link = undef;

    $mech->get($class->url);   
    my @title = ();
    my @status = ();
    my @status_modified = ();
    my @open_date = ();
    my @close_date = (); 

    # Get course by its name
    my $query = 'select fn_get_or_create_course(?, ?) ';                    
    $sth = $dbh->prepare( $query );
    $sth->execute( $class->text, 
                   'now()');
    
    $query_result = $sth->fetchrow_arrayref;
    my $pk_course = $query_result->[0];

    # Check the url and get assignment data
    if( defined $mech->find_link( text => 'Assignments' ) )
    {
        $mech->follow_link(text => 'Assignments', n => 1);
        my $html_source = $mech->content();

        my $extor = HTML::SimpleLinkExtor->new();
        $extor->parse( $html_source );

        # Parse HTML iframe to get data. 
        my @iframe_links = $extor->iframe;
        
        $mech->follow_link(url => @iframe_links);
        $iframe_data = $mech->content();

        Db_lib::extract_assignment_table( $iframe_data, \@title, \@status, \@status_modified, \@open_date, \@close_date );
        
        for( $itr=0; $itr <= $#title; $itr++ )
        {

            # Get parent
            $query = 'select assignment '
                   . '  from tb_assignment '
                   . ' where parent = null '
                   . '   and title = ? ';
            $sth = $dbh->prepare( $query );
            $sth->execute( $class->text );
            $query_result = $sth->fetchrow_arrayref;
            my $pk_assignment_parent = $query_result->[0];

            # Store information to database
            $query = 'select fn_get_or_create_assignment'
                   . '( ?,?,?,?,?,?,?,?,?)';
            $sth = $dbh->prepare( $query );
            $sth->execute( $title[$itr],
                           $status[$itr],
                           "$status_modified[$itr]::datetime",
                           "$open_date[$itr]::datetime",
                           "$close_date[$itr]::datetime",
                           $pk_course,
                           $entity,
                           'now()',
                           $pk_assignment_parent
                         );
        }
    }
}

print $entity;
#print 'Successfully';
