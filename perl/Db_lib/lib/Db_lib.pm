package Db_lib;

use Exporter();

use strict;
use warnings;

use HTML::TableExtract;
use Data::Dumper;
use Time::Piece;

# Variables here are initialized by default, or literally in this file
use vars qw( @ISA @EXPORT );
@ISA = qw( Exporter );
@EXPORT = qw( 
                 &extract_assignment
                 &trim_time
                 &trim_status
            );
$! = 1;

sub extract_assignment_table($$$$$$)
{
    my( $html_source, $title, $status, $status_modified, $open_date, $close_date ) = @_;

    # Exactract table from html file

    my $te = new HTML::TableExtract( attribs => { border => 0, cellspacing => 0} );
    $te->parse($html_source);

    my ($table) = $te->tables;

    return( 'Cannot get information from t-square' ) unless ( defined $table );

    my @rows = $table->rows;
    my $header_row = $rows[0];
    s/^\s+//, s/\s+$// for @$header_row;
   
    my $headers_hashref = {};
    my $itr = 0;

    foreach my $header (@$header_row){
        $headers_hashref->{ $header } = $itr;
        $itr++;
    }
    
    # trim leading/trailing whitespace from base fields
    foreach my $row (@rows[1 .. $#rows]) {
        s/^\s+//, s/\s+$// for @$row;
       
        my ( $my_status, $my_status_modified );

        &trim_status( $row->[ $headers_hashref->{ 'Status' }], \$my_status, \$my_status_modified );
       
        if( $my_status eq "Submitted" )
        {
            $my_status = 1;
        }
        elsif( $my_status eq "Not started" )
        {
            $my_status = 2;
        }
        elsif( $my_status eq "Returned" )
        {
            $my_status = 3;
        }

        push @{$title}, $row->[ $headers_hashref->{ 'Assignment title' } ];
        push @{$status}, $my_status;
        push @{$status_modified}, &trim_time( $my_status_modified );
        push @{$open_date}, &trim_time( $row->[ $headers_hashref->{ 'Open' } ] );
        push @{$close_date}, &trim_time( $row->[ $headers_hashref->{ 'Due' } ] );
    }
}

sub trim_time($)
{
    my( $time ) = @_;
    my $ret_time = Time::Piece->strptime( $time, '%b %d, %Y %I:%M %p' );
    return $ret_time->strftime( '%F %T' );
}

sub trim_status($$$)
{
    my ( $input, $status, $date ) = (@_);
   
    if( $input =~ /Submitted / ) 
    {
        $$date = $';
        $$status = "Submitted";
    }
    elsif( $input =~ /Returned/ )
    {
        $$date = undef;
        $$status = "Returned";
    }
    else
    {
        $$date = undef;
        $$status = "Not started";
    }

}
1;
