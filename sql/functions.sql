DROP FUNCTION IF EXISTS fn_get_entity_by_username;

DELIMITER $$
CREATE FUNCTION fn_get_entity_by_username
(
    in_username     varchar(100)
)
RETURNS integer
begin
    return 
    (
        select entity
          from tb_entity
         where username = in_username
    );
end
 $$

DROP FUNCTION IF EXISTS fn_new_entity;

CREATE FUNCTION fn_new_entity
(
    in_username         varchar(100),
    in_password         varchar(100),
    in_created          datetime
)
RETURNS integer
begin 
    declare 
        my_entity       integer;
    
    insert into tb_entity
    (
        username,
        password,
        created
    )
    values
    (
        in_username,
        in_password,
        in_created
    );

    select last_insert_id()
      into my_entity;

     return my_entity;
end
 $$

DROP FUNCTION IF EXISTS fn_get_or_create_entity;

CREATE FUNCTION fn_get_or_create_entity
(
    in_username         varchar(100),
    in_password         varchar(100),
    in_created          datetime
)
RETURNS integer(11)
begin
    declare 
        my_entity       integer(11);

    select fn_get_entity_by_username
    (
        in_username
    )
      into my_entity;

    if my_entity is null then
        select fn_new_entity
        (
            in_username,         
            in_password,         
            in_created          
        )
          into my_entity;
    end if;

    return my_entity;

end
 $$

DROP FUNCTION IF EXISTS fn_get_assignment_by_student_and_titLe;

CREATE FUNCTION fn_get_assignment_by_student_and_titLe
(
    in_student    integer,
    in_title      varchar(100)
)
RETURNS integer(11)
BEGIN
    DECLARE
        my_assignment   integer;

    select assignment
      into my_assignment
      from tb_assignment
     where student = in_student
       and title = in_title;

    RETURN my_assignment;

end
 $$

DROP FUNCTION IF EXISTS fn_new_assignment;

CREATE FUNCTION fn_new_assignment
(
    in_title                varchar(100),
    in_assignment_status    smallint(2),
    in_status_modified      datetime,
    in_open_date            datetime,
    in_close_date           datetime,
    in_course               integer(11),
    in_student              integer(11),
    in_modified             datetime,
    in_parent               integer(11)
)
RETURNS integer(11)
BEGIN
    
    DECLARE
        my_assignment       integer(11);

    insert into tb_assignment
    (
        title,               
        assignment_status,
        status_modified,  
        open_date,        
        close_date,       
        course,           
        student,            
        modified,         
        parent
    )
    values
    (
        in_title,            
        in_assignment_status,
        in_status_modified,  
        in_open_date,        
        in_close_date,       
        in_course,           
        in_student,            
        in_modified,         
        in_parent
    );

    select last_insert_id()
      into my_assignment;

    RETURN my_assignment;

END
 $$

DROP FUNCTION IF EXISTS fn_get_or_create_assignment;

CREATE FUNCTION fn_get_or_create_assignment
(
    in_title                varchar(100),   
    in_assignment_status    smallint(2),
    in_status_modified      datetime,
    in_open_date            datetime,
    in_close_date           datetime,
    in_course               integer(11),
    in_student              integer(11),
    in_modified             datetime,
    in_parent               integer(11)
)
RETURNS integer(11)
BEGIN
    DECLARE
        my_assignment       integer(11);

    select fn_get_assignment_by_student_and_titLe
    (
        in_student,
        in_title
    )
      into my_assignment;

    if my_assignment is null then
        select fn_new_assignment
        (
            in_title,               
            in_assignment_status,
            in_status_modified,  
            in_open_date,        
            in_close_date,       
            in_course,           
            in_student,            
            in_modified,         
            in_parent
        )
          into my_assignment;
    end if;

    RETURN my_assignment;

END
 $$

DROP FUNCTION IF EXISTS fn_get_course_by_name;

CREATE FUNCTION fn_get_course_by_name
(
    in_name     varchar(100)
)
RETURNS integer(11)
BEGIN
    DECLARE
        my_course   integer(11);

    select course
      into my_course
      from tb_course
     where name = in_name;

    RETURN my_course;

END
 $$

DROP FUNCTION IF EXISTS fn_new_course;

CREATE FUNCTION fn_new_course
(
    in_name     varchar(100),
    in_created  datetime
)
RETURNS integer(11)
BEGIN
    DECLARE 
        my_course   integer(11);

    insert into tb_course
    (
        name,
        created
    )
    values
    (
        in_name,      
        in_created
    );

    select last_insert_id()
      into my_course;

    RETURN my_course;

END
 $$

DROP FUNCTION IF EXISTS fn_get_or_create_course;

CREATE FUNCTION fn_get_or_create_course
(
    in_name     varchar(100),
    in_created  datetime   
)
RETURNS integer(11)
BEGIN
    DECLARE 
        my_course integer(11);

    select fn_get_course_by_name
    (
        in_name
    )
      into my_course;

    if my_course is null then 
        select fn_new_course
        (
            in_name,      
            in_created
        )
          into my_course;
    end if;

    RETURN my_course;

END
 $$


