insert into tb_entity( entity, username, password, created )
    values( -1, 'root', '297D3f00rs', NOW() );

insert into tb_assignment_status( assignment_status, label )
    values( 1, 'Submitted' ),
          ( 2, 'Not Started' );
