DROP PROCEDURE IF EXISTS fn_get_entity_by_username;

DELIMITER //
CREATE FUNCTION fn_get_entity_by_username
(
    IN  in_username     integer,
    OUT my_entity       integer
)
RETURNS integer DETERMINISTIC
language SQL
BEGIN
    declare my_entity integer(11);
    select entity
    into my_entity
    from tb_entity
    where username = in_username;

END //
