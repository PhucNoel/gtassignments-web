CREATE TABLE `tb_entity`
(
    `entity`      integer(11) not null auto_increment primary key,
    `username`    varchar(100) not null unique,
    `password`    varchar(100) not null,
    `created`     datetime not null default 0

) engine = InnoDB;

CREATE TABLE `tb_course`
(
    `course`        integer(11) not null auto_increment primary key,
    `name`          varchar(100) not null unique,
    `created`       datetime not null default 0

) engine = InnoDB;

CREATE TABLE `tb_assignment_status`
(
    `assignment_status`     smallint(2) not null primary key,
    `label`                 varchar(30) not null unique
    
) engine = InnoDB;

CREATE TABLE `tb_assignment`
(
    `assignment`        integer(11) not null auto_increment primary key,
    `title`             varchar(100) not null,
    `assignment_status` smallint(2) not null default '0',
    `status_modified`   datetime not null,
    `open_date`         datetime not null,
    `close_date`        datetime not null,
    `course`            integer not null,
    `student`           integer(11) not null,
    `modified`          datetime not null,
    `parent`            integer(11),
    
    FOREIGN KEY (parent) references tb_assignment(assignment),
    FOREIGN KEY (student) references tb_entity(entity),
    FOREIGN KEY (course) references tb_course(course)

) engine = InnoDB;
