<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>Flat Login</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--Link href="common/css/mylogin.gtassignments.css" rel="stylesheet" type="text/css"/>-->
    <link href="common/css/login_gtassignments.css" rel="stylesheet" type="text/css"/>
</head>
<body>

    <div class="container">
        <div class="flat-form">
            <div class="top-stripe">    
                <h1 align="center" >Login</h1>
                <div class="container-content">
                <p align="center">
                    Please Enter Your T-Square Login Credentials 
                </p>
                <form>
                    <ul align="center">
                        <li>
                            <input type="text" placeholder="Username" />
                        </li>
                        <li>
                            <input type="password" placeholder="Password" />
                        </li>
                        <li>
                            <input type="submit" value="Login" class="button" />
                        </li>
                    </ul>
                </form>
                </div>
            </div>
    </div>
</body>
</html>



